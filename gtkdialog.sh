﻿#! /bin/bash
Encoding=UTF-8




export MAIN_DIALOG='
<window window_position="1" title="Cosmos" icon-name="desktop-effects" resizable="false" decorated="true">


<vbox>
    <hbox>
      <frame>
        <vbox>
          <pixmap><input file>/usr/share/cosmos/desktops/ubuntu.png</input></pixmap>
          <button><label>Ubuntu</label><action>echo</action></button>
        </vbox>


        <vbox>
          <pixmap><input file>/usr/share/cosmos/desktops/apple.png</input></pixmap>
          <button><label>Apple</label><action>echo</action></button>
        </vbox>


      </frame>


      <frame>
        <vbox>
          <pixmap><input file>/usr/share/cosmos/desktops/win7.png</input></pixmap>
          <button><label>Windows7</label><action>echo</action></button>
        </vbox>


        <vbox>
          <pixmap><input file>/usr/share/cosmos/desktops/netbook.png</input></pixmap>
          <button><label>Xubuntu</label><action>echo</action></button>
        </vbox>
      </frame>
    </hbox>
  </vbox>
  </window>
  '


  gtkdialog --program=MAIN_DIALOG