Cosmos
====

This is an old script that would use zenity, xfconf, and some simple bash logic to allow a user in an XFCE enviornment to make their desktop look more like OsX, windows, or Ubuntu Unity.

This project is not being actively developed since 2010 but has been merged here for archival reasons. 

This is open source so if this can help you do something cooler or perhaps inspire you to do something coolest, please so long as the FOSS community benefits.

*nb: Iuse git-annex to manage all my data, if for some reason you run across a file that you can not see on here, just drop me an email and I'll upload whatever file you are missing.

 
