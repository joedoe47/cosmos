﻿#!/bin/sh
Encoding=UTF-8
################################################
# Xfce UI Switcher
# By: Jose A Mendoza
# License under Creative Commons v3
#        |n.b. please let me know if you plan to redistribute, remix or use for a major project
################################################
#
#         This script just make the xfce ui look like the ui you choose, it doesn't go all the way but it makes the desktop look similar if not
# familiar. Thus far this also does it's best to bring about many "features" that come native to the Ui's they try to mimic
#        !- Please submit bugs, ideas, code to joedoe47@gmail.com or at https://bugs.launchpad.net/joeos -!
#
#
#################################################
#        Credits and Props
#################################################
#
# credit to lkjoel (twitter @ lkjoel) aka MiJyn for unity like interface and some initial ideas as to how the dos (7) like os should look like
# unity source https://lkubuntu.wordpress.com/2011/09/05/make-xfce4-look-a-bit-like-unity/
# windows original source https://lkubuntu.wordpress.com/2011/09/06/make-xfce4-look-like-windows/
#
# credit to lauf for some of the code (mostly the vars and creating desktop files), props to them and thier awesome application.
# (c) 2010 joshua.redfield(AT)gmail.com
# (c) 2010 hunterm.haxxr(AT)gmail.com
################################################
#! /bin/bash
Encoding=UTF-8




export MAIN_DIALOG='
<window window_position="1" title="Cosmos" icon-name="desktop-effects" resizable="false" decorated="true">


<vbox>
    <hbox>
      <frame>
        <vbox>
          <pixmap><input file>/usr/share/cosmos/desktops/ubuntu.png</input></pixmap>
          <button><label>Ubuntu</label>
<action>sh -e -v -x /usr/share/cosmos/cosmos-cli --ubuntu</action></button>
        </vbox>


        <vbox>
          <pixmap><input file>/usr/share/cosmos/desktops/apple.png</input></pixmap>
          <button><label>Apple</label>
<action>sh -e -v -x /usr/share/cosmos/cosmos-cli --apple</action></button>
        </vbox>


      </frame>


      <frame>
        <vbox>
          <pixmap><input file>/usr/share/cosmos/desktops/win7.png</input></pixmap>
          <button><label>Windows7</label>
<action>sh -e -v -x /usr/share/cosmos/cosmos-cli --win7</action></button>
        </vbox>


        <vbox>
          <pixmap><input file>/usr/share/cosmos/desktops/netbook.png</input></pixmap>
          <button><label>Xubuntu</label>
<action>sh -e -v -x /usr/share/cosmos/cosmos-cli --ubuntu</action></button>
        </vbox>
      </frame>
    </hbox>
  </vbox>
  </window>
  '


  gtkdialog --program=MAIN_DIALOG