﻿#!/bin/sh
echo "warning this script reverts xfce to it's factory defaults"
echo "you should only use this script if there is something MAJORLY wrong with your xfce desktop"
echo -n "do you want to restore xfce to it's factory defaults? y/n" 
read response
echo
grep -i "$response"


if  [ $? == 0 ]; then
  echo "You are already registered, quitting."
  exit 1
elif [ "$response" == "y" ]; then
  killall xfce4-panel
killall xfconfd
killall xfsettingsd 
#the Xfce settings
cp -r ~/.config/xfce4-old/* ~/.config/xfce4
setsid sh -c 'cd / && xfsettingsd ... & :' > /dev/null 2>&1 < /dev/null
sleep 4
setsid sh -c 'cd / && xfce4-panel ... & :' > /dev/null 2>&1 < /dev/null
sleep 4
setsid sh -c 'cd / && xfwm4 --replace ... & :' > /dev/null 2>&1 < /dev/null


  exit 1
else
exit 1
fi