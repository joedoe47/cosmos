﻿#!/bin/sh
# Xfce UI Switcher Installer
# By: Jose A Mendoza
# License under Creative Commons v3
#    |n.b. please let me know if you plan to redistribute, remix or use for a major project
#
#     This script just make the xfce ui look like the ui you choose, it doesn't go all the way but it makes the desktop look similar if not
# familiar. Thus far this also does it's best to bring about many "features" that come native to the Ui's they try to mimic like the
# ui restore feature in mac and the edge resitance psudeo maximize on win7
#    !- Please submit bugs, ideas, code to joedoe47@gmail.com or at https://bugs.launchpad.net/joeos -!


echo "this script will install the XFCE 4 UI switcher on any ubuntu distro"
if [ `whoami` != "root" ]; then
echo "please make sure you run this as root!"
exit 0
fi
echo " "
echo "adding repos..."
add-apt-repository ppa:xubuntu-dev/xfce-4.10
add-apt-repository ppa:ricotz/docky
add-apt-repository ppa:ravefinity-project/ppa
#add-apt-repository ppa:simonschneegans/testing
#this is only needed if you plan to remaster a linux distro
#wget -O - http://www.remastersys.com/ubuntu/remastersys.gpg.key | apt-key add -
#echo "deb http://www.remastersys.com/ubuntu lucid main"| sudo tee -a /etc/apt/sources.list
#get the minimal things going
sleep 1
echo " "
echo "getting updates/upgrades..."
apt-get update
apt-get upgrade
apt-get install plank gtk2-engines radiance-xfce-lxde ambiance-xfce-lxde gnome-themes-standard gtk2-engines-xfce human-icon-theme humanity-icon-theme xfwm4-themes libgtk2.0-dev
echo " "
echo "installing cosmos..."
mkdir /usr/share/cosmos
mkdir /usr/share/cosmos/icons
mkdir /usr/share/cosmos/desktops
cp -r cosmos/* /usr/share/cosmos
ln -s /usr/share/cosmos/cosmos /usr/bin
ln -s /usr/share/cosmos/cosmos-cli /usr/bin
ln -s /usr/share/cosmos/restore-xfce /usr/bin
cp ui-switcher.desktop /usr/share/applications
chmod +x /usr/share/cosmos/cosmos
chmod +x /usr/share/cosmos/cosmos-cli
chmod +x /usr/share/cosmos/restore-xfce
chmod -R a+r /usr/share/cosmos/*
echo " "
echo "if you didn't see any warnings or errors in the terminal then your almost set..."
echo " "
echo "now installing gtkdialog..."
cd gtkdialog-0.8.2
./configure
./make
echo " "
echo "if everything went well then just type 'sudo make install' to finish everything..."
cd gtkdialog-0.8.2


exit 1
# END OF Script