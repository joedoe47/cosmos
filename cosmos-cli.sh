﻿#!/bin/sh
################################################
# Xfce UI Switcher
# By: Jose A Mendoza
# License under Creative Commons v3
#        |n.b. please let me know via email if you plan to redistribute, remix or use for a major project
################################################
#
#         This script just make the xfce ui look like the ui you choose, it doesn't go all the way but it makes the desktop look similar if not
# familiar. Thus far this also does it's best to bring about many "features" that come native to the Ui's they try to mimic.
#        !- Please submit bugs, ideas, code to joedoe47@gmail.com or at https://bugs.launchpad.net/joeos -!
#you can also comment on the this or any of the other projects I have on google docs:
# https://docs.google.com/folder/d/0B_ayFKLloMs_MjJOTUdzUTBEZnM/edit
#
#################################################
#        Credits and Props
#################################################
#
# credit to lkjoel (twitter @ lkjoel) aka MiJyn for unity like interface and some initial ideas as to how the dos (7) like os should look like.
# unity source https://lkubuntu.wordpress.com/2011/09/05/make-xfce4-look-a-bit-like-unity/
#
# windows original source https://lkubuntu.wordpress.com/2011/09/06/make-xfce4-look-like-windows/
#
#
# credit to lauf for some of the code (mostly the vars and creating desktop files), props to them and their awesome application.
# I recommend using luaf if kupfer seems
# (c) 2010 joshua.redfield(AT)gmail.com
# (c) 2010 hunterm.haxxr(AT)gmail.com
################################################
###########
###########
##vars
readonly app_dir=$(dirname $0) #Application Directory
readonly app_icon="/usr/share/cosmos/icons/desktop-effects.svg" #Application Icon
readonly dock="plank" #dock of choice
#readonly launcher="" #can’t implement due to unity launcher bug on xfce
###########
###########
##desktop launcher
desktop_file() {
echo "[Desktop Entry]
Version=1.0
Type=Application
Terminal=false
Name=UI Switcher
GenericName=User Interface Switcher
Exec=sh /usr/bin/switch-ui
Icon=$app_icon
Comment=Lets you switch between a mac, windows and xfce user interface" > "$dir_temp"
chmod +x $dir_temp
}
###########
###########
##dock files
dock_file() {
echo "[Desktop Entry]
Version=1.0
Type=Application
Terminal=false
Name=Dock
GenericName=Dock
Exec=${dock}
Icon=${dock}
Comment=" > "$dir_temp"
chmod +x $dir_temp
}
###########
###########
##unity files
unity_file() {
echo "[Desktop Entry]
Version=1.0
Type=Application
Terminal=false
Name=Unity Launcher
GenericName=Launcher
Exec=${launcher}
Icon=${launcher}
Comment=" > "$dir_temp"
chmod +x $dir_temp
}
###########
###########
##blank file
blank_file() {
echo "" > "$dir_temp"
chmod +x $dir_temp
}
###########
###########
##Settings applied to all or most Desktops
common_conf () {
#these need to be killed other wise some settings won't apply until you logout and back in
killall $dock &
killall unity-2d-shell &
killall xfce4-panel &
xfconf-query -c xsettings -np '/Gtk/FontName' -t 'string' -ns "Ubuntu 10" &
xfconf-query -c xsettings -np '/Net/EnableEventSounds' -t 'bool' -ns "true" &
xfconf-query -c xsettings -np '/Net/EnableInputFeedbackSounds' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/click_to_focus' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/cycle_apps_only' -t 'bool' -ns "false" &
xfconf-query -c xfwm4 -np '/general/urgent_blink' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/unredirect_overlays' -t 'bool' -ns "true" &
xfconf-query -c xfce4-session -np '/security/EnableTcp' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/length' -t 'int' -ns "100" &
xfconf-query -c xfce4-panel -np '/panels/panel-1/length' -t 'int' -ns "100" &
sleep 1
xfconf-query -c xfce4-panel -np '/panels/panel-0/length-adjust' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/position-locked' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/span-monitors' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/background-style' -t 'int' -ns "0" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/horizontal' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/length-adjust' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/show-tooltips' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-3' -t 'string' -ns "separator" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-3/expand' -nt 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-4' -t 'string' -ns "systray" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-4'show-frame -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-5' -t 'string' -ns "indicator" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-6' -t 'string' -ns "separator" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-6/style' -t 'int' -ns "0" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-7/digital-format' -t 'string' -ns "%a, %d %b %H:%M" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-7/tooltip-format' -t 'string' -ns "%A %d %B %Y %H:%M:%S" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-7/show-frame' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-3/style' -t 'int' -ns "0" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-26' -t 'string' -ns "pager" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-26/miniature-view' -t 'bool' -ns "true" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-26/rows' -t 'int' -ns "2" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-26/show-names' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-26/workspace-scrolling' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/panels/panel-1/output-name' -t 'string' -ns "DIE" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-25' -t 'string' -ns "directorymenu" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-24' -t 'string' -ns "thunar-tpa" &
xfconf-query -c xfce4-panel -np '/panels/panel-1/background-style' -t 'int' -ns "0" &
}
###########
###########
##Restart the apps
Restart_apps () {
#used to need to do this sometimes unreliable command after finishing
#xfsettingsd &
#xfwm4 --replace ;
        xfsettingsd &
        xfce4-panel &
}
###########
###########
##Apple
Apple_ui () {
 notify-send -u critical -i "desktop" 'Transforming...' 'Your desktop is being setup!'
                    dir_temp="$HOME/.config/autostart/dock.desktop"
                        dock_file
        #change xfconf start
common_conf
xfconf-query -c xsettings -np '/Net/ThemeName' -t 'string' -ns "Greybird" &
xfconf-query -c xfwm4 -np '/general/theme' -t 'string' -ns "Greybird" &
xfconf-query -c xsettings -np '/Net/IconThemeName' -t 'string' -ns "elementaryXubuntu" &
xfconf-query -c xsettings -np '/Net/SoundThemeName' -t 'string' -ns "default" &
xfconf-query -c xsettings -np '/Gtk/ButtonImages' -t 'bool' -ns "true" &
xfconf-query -c xsettings -np '/Gtk/MenuImages' -t 'bool' -ns "true" &
xfconf-query -c xsettings -np '/Gtk/ToolbarStyle' -t 'string' -ns "icons" &
sleep 1
xfconf-query -c xfwm4 -np '/general/borderless_maximize' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/button_layout' -t 'string' -ns "CMH|O" &
xfconf-query -c xfwm4 -np '/general/use_compositing' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/workspace_count' -t 'int' -ns "4" &
xfconf-query -c xfwm4 -np '/general/wrap_windows' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/wrap_workspaces' -t 'bool' -ns "true" &
sleep 1
xfconf-query -c xfce4-session -np '/general/SaveOnExit' -t 'bool' -ns "true" &
xfconf-query -c xfce4-notifyd -np '/notify-location' -t 'string' -ns "2" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/background-alpha' -t 'int' -ns "77" &
sleep 1
xfconf-query -c xfce4-panel -np '/panels/panel-0/position' -t 'string' -ns "p=6;x=0;y=0" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/size' -t 'int' -ns "24" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1' -t 'string' -ns "applicationsmenu" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/button-icon' -t 'string' -ns "/usr/share/cosmos/amenu.png" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/button-title' -t 'string' -ns "" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/show-button-title' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2' -t 'string' -ns "separator" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/expand' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/style' -t 'int' -ns "0" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-9' -t 'string' -ns "actions" &
sleep 1
xfconf-query -c xfce4-session -np '/general/SaveOnExit' -t 'bool' -ns "true" &
#change xfconf end
        Restart_apps
        $dock &
        exit 1
}
###########
###########
##7
Win7_ui () {
notify-send -u critical -i "desktop" 'Transforming...' 'Your desktop is being setup!'
                    dir_temp="$HOME/.config/autostart/dock.desktop"
                        blank_file
        #change xfconf start
common_conf
xfconf-query -c xsettings -np '/Net/ThemeName' -t 'string' -ns "blue7" &
xfconf-query -c xfwm4 -np '/general/theme' -t 'string' -ns "Default-4.6" &
xfconf-query -c xfce4-notifyd -np '/theme' -t 'string' -ns "default" &
xfconf-query -c xsettings -np '/Net/IconThemeName' -t 'string' -ns "nuoveXT.2.2" &
xfconf-query -c xsettings -np '/Net/SoundThemeName' -t 'string' -ns "default" &
xfconf-query -c xsettings -np '/Gtk/ButtonImages' -t 'bool' -ns "false" &
xfconf-query -c xsettings -np '/Gtk/MenuImages' -t 'bool' -ns "true" &
xfconf-query -c xsettings -np '/Gtk/ToolbarStyle' -t 'string' -ns "icons" &
sleep 1
xfconf-query -c xfwm4 -np '/general/borderless_maximize' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/button_layout' -t 'string' -ns "O|HMC" &
xfconf-query -c xfwm4 -np '/general/use_compositing' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/workspace_count' -t 'int' -ns "2" &
xfconf-query -c xfwm4 -np '/general/wrap_windows' -t 'bool' -ns "false" &
xfconf-query -c xfwm4 -np '/general/wrap_workspaces' -t 'bool' -ns "false" &
sleep 1
xfconf-query -c xfce4-session -np '/general/SaveOnExit' -t 'bool' -ns "false" &
xfconf-query -c xfce4-notifyd -np '/notify-location' -t 'string' -ns "2" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/background-alpha' -t 'int' -ns "100" &
sleep 1
xfconf-query -c xfce4-panel -np '/panels/panel-0/position' -t 'string' -ns "p=10;x=0;y=0" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/size' -t 'int' -ns "36" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/background-style' -t 'int' -ns "2" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/background-image' -t 'string' -ns "/usr/share/cosmos/panel_basic.png" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1' -t 'string' -ns "applicationsmenu" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/button-icon' -t 'string' -ns "/usr/share/cosmos/wmenu.png" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/show-button-title' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/button-title' -t 'string' -ns "Start" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2' -t 'string' -ns "tasklist" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/include-all-monitors' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/expand' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/flat-buttons' -t 'bool' -ns "true" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/show-labels' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-4/show-frame' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-6/style' -t 'string' -ns "0" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-9' -t 'string' -ns "showdesktop" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-26/miniature-view' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-26/rows' -t 'int' -ns "1" &
xfconf-query -c xfce4-session -np '/general/SaveOnExit' -t 'bool' -ns "false" &
#change xfconf end
        Restart_apps
        exit 1
}
###########
###########
##Ubuntu-ish Desktop
Ubuntu_ui () {
notify-send -u critical -i "desktop" 'Transforming...' 'Your desktop is being setup!'
                    dir_temp="$HOME/.config/autostart/dock.desktop"
                        unity_file
        #change xfconf start
common_conf
xfconf-query -c xsettings -np '/Net/ThemeName' -t 'string' -ns "ambiance-xfce-lxde" &
xfconf-query -c xfwm4 -np '/general/theme' -t 'string' -ns "ambiance-xfce-lxde" &
xfconf-query -c xfce4-notifyd -np '/theme' -t 'string' -ns "default" &
xfconf-query -c xsettings -np '/Net/IconThemeName' -t 'string' -ns "Humanity-Dark" &
xfconf-query -c xsettings -np '/Net/SoundThemeName' -t 'string' -ns "default" &
xfconf-query -c xsettings -np '/Gtk/ButtonImages' -t 'bool' -ns "true" &
xfconf-query -c xsettings -np '/Gtk/MenuImages' -t 'bool' -ns "true" &
xfconf-query -c xsettings -np '/Gtk/ToolbarStyle' -t 'string' -ns "icons" &
sleep 1
xfconf-query -c xfwm4 -np '/general/borderless_maximize' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/button_layout' -t 'string' -ns "CMH|O" &
xfconf-query -c xfwm4 -np '/general/use_compositing' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/workspace_count' -t 'int' -ns "4" &
xfconf-query -c xfwm4 -np '/general/wrap_windows' -t 'bool' -ns "false" &
xfconf-query -c xfwm4 -np '/general/wrap_workspaces' -t 'bool' -ns "false" &
sleep 1
xfconf-query -c xfce4-session -np '/general/SaveOnExit' -t 'bool' -ns "false" &
xfconf-query -c xfce4-notifyd -np '/notify-location' -t 'string' -ns "2" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/background-alpha' -t 'int' -ns "95" &
xfconf-query -c xfce4-panel -p '/panels/panel-1/output-name' -r
xfconf-query -c xfce4-panel -np '/panels/panel-1/position' -t 'string' -ns "p=5;x=24;y=63" &
xfconf-query -c xfce4-panel -np '/panels/panel-1/size' -t 'int' -ns "45" &
xfconf-query -c xfce4-panel -np '/panels/panel-1/background-alpha' -t 'int' -ns "48" &
sleep 1
xfconf-query -c xfce4-panel -np '/panels/panel-0/position' -t 'string' -ns "p=6;x=0;y=0" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/size' -t 'int' -ns "24" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1' -t 'string' -ns "applicationsmenu" &
xfconf-query -c xfce4-session -np '/general/SaveOnExit' -t 'bool' -ns "false" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/button-icon' -t 'string' -ns "distributor-logo" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/show-button-title' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/button-title' -t 'string' -ns "Menu" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2' -t 'string' -ns "separator" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/expand' -nt 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/style' -t 'int' -ns "0" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-6/style' -t 'string' -ns "0" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-9' -t 'string' -ns "actions" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-11' -t 'string' -ns "separator" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-11/expand' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-11/style' -t 'int' -ns "1" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-21' -t 'string' -ns "separator" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-21/expand' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-21/style' -t 'int' -ns "0" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-22' -t 'string' -ns "tasklist" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-23' -t 'string' -ns "separator" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-23/expand' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-23/style' -t 'int' -ns "0" &
#change xfconf end
        Restart_apps
        $launcher&
        exit 1
}
###########
###########
##Netbook small screen optimized desktop
xfce4_ui () {
notify-send -u critical -i "desktop" 'Transforming...' 'Your desktop is being setup!'
                    dir_temp="$HOME/.config/autostart/dock.desktop"
                        blank_file
common_conf
xfconf-query -c xsettings -np '/Gtk/FontName' -t 'string' -ns "Ubuntu 10" &
xfconf-query -c xsettings -np '/Net/ThemeName' -t 'string' -ns "greybird" &
xfconf-query -c xfwm4 -np '/general/theme' -t 'string' -ns "greybird" &
xfconf-query -c xsettings -np '/Net/IconThemeName' -t 'string' -ns "elementaryXubuntu" &
xfconf-query -c xsettings -np '/Net/SoundThemeName' -t 'string' -ns "default" &
xfconf-query -c xsettings -np '/Gtk/ButtonImages' -t 'bool' -ns "false" &
xfconf-query -c xsettings -np '/Gtk/MenuImages' -t 'bool' -ns "true" &
xfconf-query -c xsettings -np '/Gtk/ToolbarStyle' -t 'string' -ns "icons" &
xfconf-query -c xfwm4 -np '/general/borderless_maximize' -t 'bool' -ns "true" &
sleep 1
xfconf-query -c xfwm4 -np '/general/button_layout' -t 'string' -ns "CMH|O" &
xfconf-query -c xfwm4 -np '/general/click_to_focus' -t 'bool' -ns "false" &
xfconf-query -c xfwm4 -np '/general/use_compositing' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/workspace_count' -t 'int' -ns "2" &
xfconf-query -c xfwm4 -np '/general/wrap_windows' -t 'bool' -ns "true" &
xfconf-query -c xfwm4 -np '/general/wrap_workspaces' -t 'bool' -ns "true" &
xfconf-query -c xfce4-session -np '/general/SaveOnExit' -t 'bool' -ns "true" &
xfconf-query -c xfce4-notifyd -np '/notify-location' -t 'string' -ns "2" &
sleep 1
xfconf-query -c xfce4-panel -np '/panels/panel-0/background-alpha' -t 'int' -ns "77" &
xfconf-query -c xfce4-panel -p '/panels/panel-1/output-name' -r
xfconf-query -c xfce4-panel -np '/panels/panel-0/position' -t 'string' -ns "p=6;x=0;y=0" &
xfconf-query -c xfce4-panel -np '/panels/panel-0/size' -t 'int' -ns "24" &
xfconf-query -c xfce4-panel -np '/panels/panel-1/position' -t 'string' -ns "p=10;x=0;y=0" &
xfconf-query -c xfce4-panel -np '/panels/panel-1/size' -t 'int' -ns "42" &
xfconf-query -c xfce4-panel -np '/panels/panel-1/background-alpha' -t 'int' -ns “20” &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1' -t 'string' -ns "applicationsmenu" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/button-icon' -t 'string' -ns "xubuntu-logo-menu" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-1/show-button-title' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2' -t 'string' -ns "tasklist" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/include-all-monitors' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/expand' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/flat-buttons' -t 'bool' -ns "true" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-2/show-labels' -t 'bool' -ns "true" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-6/style' -t 'string' -ns "0" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-9' -t 'string' -ns "actions" &
xfconf-query -c xfce4-session -np '/general/SaveOnExit' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-11' -t 'string' -ns "separator" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-11/expand' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-11/style' -t 'int' -ns "1" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-21' -t 'string' -ns "separator" &
sleep 1
xfconf-query -c xfce4-panel -np '/plugins/plugin-21/expand' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-21/style' -t 'int' -ns "0" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-22' -t 'string' -ns "separator" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-22/expand' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-22/style' -t 'int' -ns "1" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-23' -t 'string' -ns "separator" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-23/expand' -t 'bool' -ns "false" &
xfconf-query -c xfce4-panel -np '/plugins/plugin-23/style' -t 'int' -ns "0" &
#change xfconf end
        Restart_apps
        exit 1
}
###########
###########
#CLI
case $1 in
"-l" | "--launcher")
if [ ! -n "$2" ]; then
    echo "Creating launcher in $HOME/Desktop/"
    dir_temp="$HOME/Desktop/cosmos.desktop"
    desktop_file
else
    if [ -d $2 ]; then
    dir_temp="$2/cosmos.desktop"
    echo "Creating launcher in $2"
    desktop_file
    else
        echo "Could not create launcher. $2 is not a valid directory, Sorry."
    fi
fi
exit
;;
"-d" | "--debug")
    echo "Starting in Debug Mode"
    echo "-------------------"
    set -v
    main
;;
"-h" | "--help")
    echo "Usage: cosmos, aka ui switcher [ OPTION ]"
    echo "       -d --debug  run lauf in debug mode"
    echo "       -v --version Show version"
    echo "       -h --help   show this message"
    echo "       -l --launcher [Directory]"
    echo "       -------other options-------------------"
    echo "       --apple   switch to a familiar apple desktop"
    echo "       --win7   switch to a familiar windows7 desktop"
    echo "       --netbook   switch to a netbook freindly desktop"
    echo "       --ubuntu   switch to an ubuntu desktop"
    exit 1
;;
"-v" | "--verison")
    echo "Cosmos ui-switcher version 1.0-6"
    exit 1
;;
"-ubuntu" | "--ubuntu")
    Ubuntu_ui
    exit 1
;;
"-xubuntu" | "--xubuntu")
    xfce4_ui
    exit 1
;;
"-apple" | "--apple")
    Apple_ui
    exit 1
;;
"-win7" | "--win7")
    Win7_ui
    exit 1
;;
esac
###########
###########
#The main
TITLE="Cosmos Desktop Switcher"
Main () {
        CHOICE=$(zenity --list --title "$TITLE" --hide-column 1 --text "Which desktop would you like to use?" --column "" --column "" --height="215" \
"2" "                        Ubuntu" \
"3" "                        Xubuntu" \
"1" "                        Apple" \
"0" "                        Windows 7")
        if [ $CHOICE = 0 ]; then
                Win7_ui
        fi
        if [ $CHOICE = 1 ]; then
                Apple_ui
        fi
        if [ $CHOICE = 3 ]; then
                xfce4_ui
        fi
        if [ $CHOICE = 2 ]; then
                Ubuntu_ui
        fi
}
###########
###########
##Start and End of everything
Main
# END OF Script